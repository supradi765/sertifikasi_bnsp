<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;

    protected $table = 'produk';
    public $timestamp = true;

    protected $fillable = [
       'nama_produk',
       'kode_produk',
       'harga_beli',
       'harga_jual',
       'status',
       'stock',
       'id_kelompok_produk'
    ];
}
