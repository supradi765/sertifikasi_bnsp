<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KelompokProduk;

class KelompokProdukController extends Controller
{
    public function index(){
        $kelompok = KelompokProduk::select('id', 'nama_kelompok_produk')->get();
        return view('kelompok.kelompok_produk', compact('kelompok'));
    }

    public function formGroup(){
        return view('kelompok.form');
    }

    public function simpanGroup(Request $request){
        try {
            $data = $request->all();
            $kelompok = new KelompokProduk;
            $kelompok -> nama_kelompok_produk = $data['nama_kelompok_produk'];
            $kelompok-> save();
            return redirect()->route('kelompok-produk')->with('success', __('Berhasil'));
        }catch (\Throwable $th) {
            return redirect()->route('form-group')->with('error', __($th->getMessage()));
         }
    }
    public function groupEdit($id){
        $getDataById = KelompokProduk::select('id','nama_kelompok_produk',)->where('id', $id)->first();
        return view('kelompok.halaman_edit', compact('getDataById'));
    }


    public function simpanEditGroup(Request $req, $id)
        {
            try {
                $datas = $req->all();
                KelompokProduk::where('id', $id)->update([
                    'nama_kelompok_produk' => $datas['nama_kelompok_produk'],
                ]);
                return redirect()->route('kelompok-produk')->with('success', __('Berhasil edit data'));
            } catch (\Throwable $th) {
                return redirect()->route('kelompok-produk')->with('error', __($th->getMessage()));
            }
        }

        public function deleteDataGroup($id){
            try{
                KelompokProduk::where('id', $id)->delete();

                return redirect()->route('kelompok-produk')->with('success', __('Berhasil hapus data'));
            }catch (\Throwable $th) {
                return redirect()->route('kelompok-produk', $id)->with('error', __($th->getMessage()));
            }
        }

        public function searchKelompok(Request $request)
        {
            $keyword = $request->search;

            $result = KelompokProduk::where('nama_kelompok_produk', 'like', '%' . $keyword . '%')
                ->get();
            return view('kelompok.result', [
                'keyword' => $keyword,
                'kelompok' => $result
            ]);
        }

}
