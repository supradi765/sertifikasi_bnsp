<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\KelompokProduk;
use PDF;

class ProductController extends Controller
{
    public function index(){
        $produk = Produk::select( 'produk.id', 'produk.nama_produk','produk.kode_produk','produk.harga_beli','produk.harga_jual','produk.status','produk.stock','kelompok_produk.nama_kelompok_produk')
        ->join('kelompok_produk', 'kelompok_produk.id', '=', 'produk.id_Kelompok_produk')->get();


        return view('product.list', compact('produk'));
    }


    public function form(){
        $kelompokProduk = KelompokProduk::select('id', 'nama_kelompok_produk')->get();
        return view('product.form', compact('kelompokProduk'));
    }
    public function simpanData(Request $request){
        try {
            $datas = $request->all();
            $produk = new Produk;
            $produk->nama_produk = $datas['nama_produk'];
            $produk->kode_produk = $datas['kode_produk'];
            $produk->harga_beli = $datas['harga_beli'];
            $produk->harga_jual = $datas['harga_jual'];
            $produk->status = $datas['status'];
            $produk -> id_kelompok_produk = $datas['id_kelompok_produk'];
            $produk->stock = $datas['stock'];
            $produk->save();
            return redirect()->route('list-produk')->with('success', __('Berhasil'));
        } catch (\Throwable $th) {
            return redirect()->route('form-input')->with('error', __($th->getMessage()));
        }
    }

    public function formEdit($id){
        $getDataById = Produk::select('id','nama_produk','kode_produk','harga_beli','harga_jual','status','stock',)->where('id', $id)->first();
        $kelompokProduk = KelompokProduk::select('id', 'nama_kelompok_produk')->get();
        return view('product.halaman_edit', compact('getDataById', 'kelompokProduk'));
    }

    public function editData(Request $req, $id){
        try {
            $datas = $req->all();

            Produk::where('id', $id)->update(
                [
                    'nama_produk' => $datas['nama_produk'],
                    'kode_produk' => $datas['kode_produk'],
                    'harga_beli' =>$datas['harga_beli'],
                    'harga_jual' =>$datas['harga_jual'],
                    'status' =>$datas['status'],
                    'id_kelompok_produk' =>$datas['id_kelompok_produk'],
                    'stock' =>$datas['stock'],
                ]
                );
                return redirect()->route('list-produk')->with('success', __('Berhasil edit data'));
        }catch (\Throwable $th) {
            return redirect()->route('form-edit', $id)->with('error', __($th->getMessage()));
        }
    }

    public function deleteData($id){
        try{
            Produk::where('id', $id)->delete();

            return redirect()->route('list-produk')->with('success', __('Berhasil hapus data'));
        }catch (\Throwable $th) {
            return redirect()->route('list-produk', $id)->with('error', __($th->getMessage()));
        }
    }

    public function search(Request $request)
    {
        {
            $keyword = $request->search;

            $result = Produk::select('produk.id', 'produk.nama_produk', 'produk.kode_produk', 'produk.harga_beli', 'produk.harga_jual', 'produk.status', 'produk.stock', 'kelompok_produk.nama_kelompok_produk')
                ->join('kelompok_produk', 'kelompok_produk.id', '=', 'produk.id_kelompok_produk')
                ->where('kelompok_produk.nama_kelompok_produk', 'like', '%' . $keyword . '%')
                ->orwhere('nama_produk', 'like', '%' . $keyword . '%')
                ->orwhere('kode_produk', 'like', '%' .$keyword . '%')
                ->orwhere('harga_beli', 'like', '%' .$keyword . '%')
                ->orwhere('harga_jual', 'like', '%' .$keyword . '%')
                ->orwhere('status', 'like', '%' . $keyword . '%')
                ->orwhere('stock', 'like', '%' .$keyword . '%')
                ->get();
            return view('product.result', [
                'keyword' => $keyword,
                'produk' => $result,
            ]);
        }
    }


    public function cetakPdf()
    {
    	$produk = Produk::select( 'produk.id', 'produk.nama_produk','produk.kode_produk','produk.harga_beli','produk.harga_jual','produk.status','produk.stock','kelompok_produk.nama_kelompok_produk')
        ->join('kelompok_produk', 'kelompok_produk.id', '=', 'produk.id_Kelompok_produk')->get();

    	$pdf = PDF::loadview('product.produk_pdf',['produk'=>$produk])->setOptions(['defaultFont' => 'sans-serif']);

    	return $pdf->stream();
    }
}
