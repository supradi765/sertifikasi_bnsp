<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pengguna;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index(){
        return view('auth');
    }

    public function simpanRegister(Request $req)
    {
        try {
            $this->validate($req, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|unique:pengguna',
                'password' => 'required|string|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]+$/',
            ], [
                'password.regex' => 'Password harus terdiri dari minimal 8 karakter dengan kombinasi huruf kecil, huruf besar, angka, dan karakter khusus.'
            ]);

            $datas = $req->all();
            $save = new Pengguna;
            $save->name = $datas['name'];
            $save->email = $datas['email'];
            $save->password = Hash::make($datas['password']);
            $save->save();

            return redirect()->route('auth')->with($datas['name']);
        } catch (\Throwable $th) {
            return redirect()->route('auth')->with('error', __($th->getMessage()));
        }
    }


    public function ProsesLogin(Request $req)
    {
        request()->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $kredensial = $req->only('email', 'password');

        if (Auth::attempt($kredensial)) {
            return redirect()->route('list-produk');
        }

        return redirect('/')
            ->withInput()
            ->withErrors(['login_gagal' => 'kombinasi email dan sandi salah!.']);
    }
    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();
        return Redirect('/');
    }

}
