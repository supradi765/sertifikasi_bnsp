<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\KelompokProdukController;
use App\Http\Controllers\LupaPasswordController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::controller(AuthController::class)->group(function () {
    Route::get('/', 'index')->name('auth');
    Route::post('/simpan-register', 'simpanRegister')->name('simpan-register');
    Route::post('/proses-login', 'ProsesLogin')->name('proses-login');
    Route::get('/logout', 'logout')->name('proses')->name('proses-logout');
});

Route::controller(ProductController::class)->middleware('CekLogin')->group(function () {
    Route::get('/list', 'index')->name('list-produk');
    Route::get('/form', 'form')->name('form-input');
    Route::post('/simpan', 'simpanData')->name('simpan-data');
    Route::get('/edit/{id}', 'formEdit')->name('form-edit');
    Route::post('/simpanedit/{id}', 'editData')->name('simpan-edit');
    Route::get('/hapus/{id}', 'deleteData')->name('hapus-data');
    Route::get('search', 'search')->name('search');
    Route::get('cetakpdf', 'cetakPdf')->name('cetak_pdf');
});



Route::controller(KelompokProdukController::class)->middleware('CekLogin')->group(function () {
    Route::get('/group', 'index')->name('kelompok-produk');
    Route::get('/form_group', 'formGroup')->name('form-group');
    Route::post('/simpan_group', 'simpanGroup')->name('simpan-group');
    Route::get('/edit_group/{id}', 'groupEdit')->name('edit-group');
    Route::post('/simpan_group_edit/{id}', 'simpanEditGroup')->name('simpan-edit-group');
    Route::get('/hapus_group/{id}', 'deleteDataGroup')->name('hapus-data-group');
    Route::get('/searchkelompok', 'searchKelompok')->name('search-kelompok');
});

Route::controller(LupaPasswordController::class)->name('forgot.')->prefix('forgot')->group(function () {
    Route::get('/', 'formLupapassword')->name('form-forgot');
    Route::post('/proses', 'prosesLupaPassword')->name('proses');
    Route::get('/reset-password/{token}', 'resetPassword')->name('reset-password');
    Route::post('/proses-reset', 'prosesResetPassword')->name('proses-reset');
});
