<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Landing Page</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
      <a class="navbar-brand text-light" href="#">Logo</a>
    </nav>
  </header>

  <div class="d-flex justify-content-center align-items-center" style="height: 100vh;">
    <div>
      <h3>
        @if($errors->any())
        @foreach($errors->all() as $error)
          <div class="alert alert-danger" role="alert">
            {{ $error }}
          </div>
        @endforeach
      @endif

      @if(session('error'))
        <div class="alert alert-danger">
          {{ session('error') }}
        </div>
      @endif
      </h3>

      <div class="text-center">
        <h2>Silahkan daftar</h2>
      </div>
      <form action="{{ route('simpan-register') }}" method="post">
        @csrf
        <div class="form-group">
          <label for="signupName">Name</label>
          <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" placeholder="Enter your name">
        </div>
        <div class="form-group">
          <label for="signupEmail">Email address</label>
          <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}" placeholder="Enter email">
        </div>
        <div class="form-group">
          <label for="signupPassword">Password</label>
          <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}" placeholder="Password">
        </div>
        <div class="d-flex justify-content-between">
          <button type="submit" class="btn btn-primary">Sign Up</button>
          <div>
            <p>Already have an account? <a href="#" class="text-primary" data-toggle="modal" data-target="#loginModal">Click here to login</a></p>
          </div>
        </div>
      </form>
    </div>
  </div>

  <!-- ... kode HTML setelahnya ... -->

  <!-- Modal -->

  <!-- Modal -->
  <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="loginModalLabel">Silahkan Login</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ route('proses-login') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="email">Email address</label>
              <input type="email" class="form-control" id="email" value="{{ old('email') }}" name="email" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" class="form-control" id="password" value="{{ old('password') }}" name="password" placeholder="Password">
            </div>
            <div class="form-group">
                <a href="{{ route('forgot.form-forgot') }}">Lupa Password?</a>
            </div>
            <button type="submit" class="btn btn-primary">Login</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.3/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
