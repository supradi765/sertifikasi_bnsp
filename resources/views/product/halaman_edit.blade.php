<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">
    <!-- Custom CSS -->
    <style>
        .card {
            margin-top: 50px;
        }
        .card-header {
            background-color: #f8f9fa;
            font-weight: bold;
        }
        .btn-primary {
            background-color: #007bff;
            border-color: #007bff;
        }
        .btn-primary:hover {
            background-color: #0069d9;
            border-color: #0062cc;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title text-center">Edit Produk</h3>

                    </div>
                    <div class="card-body">
                        <form action="{{ url('/simpanedit'.'/'. $getDataById->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <label for="nama_produk" class="form-label">Nama Produk</label>
                                <input type="text" class="form-control" name="nama_produk" id="nama_produk" value="{{ $getDataById->nama_produk }}">
                            </div>
                            <div class="mb-3">
                                <label for="kode_produk" class="form-label">Kode Produk</label>
                                <input type="text" class="form-control" name="kode_produk" id="kode_produk" value="{{ $getDataById->kode_produk }}">
                            </div>
                            <div class="mb-3">
                                <label for="harga_beli" class="form-label">Harga Beli</label>
                                <input type="number" class="form-control" name="harga_beli" id="harga_beli" value="{{ $getDataById->harga_beli }}">
                            </div>
                            <div class="mb-3">
                                <label for="harga_jual" class="form-label">Harga Jual</label>
                                <input type="number" class="form-control" name="harga_jual" id="harga_jual" value="{{ $getDataById->harga_jual }}">
                            </div>
                            <div class="mb-3">
                                <label for="status" class="form-label">Status Produk</label>
                                <select class="form-control" name="status" id="status">
                                    <option value="tunai" {{ $getDataById->status === 'tunai' ? 'selected' : '' }}>Tunai</option>
                                    <option value="kredit" {{ $getDataById->status === 'kredit' ? 'selected' : '' }}>Kredit</option>
                                    <option value="konsinyasi" {{ $getDataById->status === 'konsinyasi' ? 'selected' : '' }}>Konsinyasi</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="id_kelompok_produk" class="form-label">Kelompok Produk</label>
                                <select class="form-control" name="id_kelompok_produk" id="id_kelompok_produk">
                                    @foreach($kelompokProduk as $kelompok)
                                        <option value="{{ $kelompok->id }}" {{ $getDataById->id_kelompok_produk == $kelompok->id ? 'selected' : '' }}>{{ $kelompok->nama_kelompok_produk }}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="mb-3">
                                <label for="stock" class="form-label">Stock</label>
                                <input type="number" class="form-control" name="stock" id="stock" value="{{ $getDataById->stock }}">
                            </div>
                            <div class="d-flex justify-content-between">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="submit" class="btn btn-primary"><a href="{{ route('list-produk') }}" class="text-decoration-none text-white">kembali</a></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
