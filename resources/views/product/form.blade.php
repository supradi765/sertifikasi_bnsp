<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title text-center">Tambah Produk</h3>
                        <a href="{{ url('form_group') }}" class="btn btn-secondary"><i class="material-icons">&#xE147;</i> <span>Tambah kelompok produk</span></a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('simpan-data') }}" method="post">
                            @csrf
                            <div class="mb-3">
                                <label for="nama_produk" class="form-label">Nama Produk : max 12 karakter</label>
                                <input type="text" class="form-control" name="nama_produk" id="nama_produk" value="{{ old('nama_produk') }}">
                            </div>
                            <div class="mb-3">
                                <label for="kode_produk" class="form-label">Kode Produk : max 30 karakter</label>
                                <input type="text" class="form-control" name="kode_produk" id="kode_produk" value="{{ old('kode_produk') }}">
                            </div>
                            <div class="mb-3">
                                <label for="harga_beli" class="form-label">Harga Beli</label>
                                <input type="number" class="form-control" name="harga_beli" id="harga_beli" value="{{ old('harga_beli') }}">
                            </div>
                            <div class="mb-3">
                                <label for="harga_jual" class="form-label">Harga Jual</label>
                                <input type="number" class="form-control" name="harga_jual" id="harga_jual" value="{{ old('harga_jual') }}">
                            </div>
                            <div class="mb-3">
                               <select name="id_kelompok_produk" id="id_kelompok_produk">
                                    <option value="">kelompok produk</option>
                                    @foreach ($kelompokProduk as $key )
                                        <option value="{{ $key->id }}">{{ $key->nama_kelompok_produk }}</option>
                                    @endforeach
                               </select>
                            </div>
                            <div class="mb-3">
                                <label for="status" class="form-label">Status Produk</label>
                                <select class="form-select" name="status" id="status">
                                    <option value="tunai" {{ old('status') === 'tunai' ? 'selected' : '' }}>Tunai</option>
                                    <option value="kredit" {{ old('status') === 'kredit' ? 'selected' : '' }}>Kredit</option>
                                    <option value="konsinyasi" {{ old('status') === 'konsinyasi' ? 'selected' : '' }}>Konsinyasi</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="stock" class="form-label">Stock</label>
                                <input type="number" class="form-control" name="stock" id="stock" value="{{ old('stock') }}">
                            </div>
                            <div class="d-flex justify-content-between">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="submit" class="btn btn-primary"><a href="{{ route('list-produk') }}" class="text-decoration-none text-white">kembali</a></button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Ambil referensi elemen-elemen formulir
            var form = document.querySelector("form");
            var namaProdukInput = document.getElementById("nama_produk");
            var kodeProdukInput = document.getElementById("kode_produk");
            var hargaBeliInput = document.getElementById("harga_beli");
            var hargaJualInput = document.getElementById("harga_jual");
            var kelompokProdukSelect = document.getElementById("id_kelompok_produk");
            var statusProdukSelect = document.getElementById("status");
            var stockInput = document.getElementById("stock");

            // Fungsi untuk memeriksa apakah elemen input/select sudah diisi
            function isElementFilled(element) {
                return element.value.trim() !== "";
            }

            // Fungsi untuk memeriksa panjang karakter maksimal
            function isElementMaxLength(element, maxLength) {
                return element.value.length <= maxLength;
            }

            // Fungsi untuk memeriksa validitas formulir saat pengiriman
            function validateForm(event) {
                if (!isElementFilled(namaProdukInput) ||
                    !isElementFilled(kodeProdukInput) ||
                    !isElementFilled(hargaBeliInput) ||
                    !isElementFilled(hargaJualInput) ||
                    kelompokProdukSelect.value === "" ||
                    statusProdukSelect.value === "" ||
                    !isElementFilled(stockInput) ||
                    !isElementMaxLength(namaProdukInput, 12) ||
                    !isElementMaxLength(kodeProdukInput, 30)
                ) {
                    event.preventDefault(); // Mencegah pengiriman formulir jika ada data yang kosong atau melebihi batas karakter maksimal
                    alert("Harap isi semua data yang wajib dan pastikan input tidak melebihi batas karakter maksimal!"); // Tampilkan pesan peringatan
                }
            }

            // Tambahkan event listener ke formulir saat pengiriman
            form.addEventListener("submit", validateForm);
        });
    </script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</body>
</html>
